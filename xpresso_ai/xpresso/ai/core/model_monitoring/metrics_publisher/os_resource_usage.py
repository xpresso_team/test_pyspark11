"""
    Module for OS resource usage statistics
"""

__all__ = ["OSResourceUsage"]
__author__ = ["Shlok Chaudhari"]


import psutil
import GPUtil

from xpresso.ai.core.commons.utils.constants import MIN_INTERVAL, \
    LAST_ELEMENT_INDEX, CONV_FACTOR, DEFAULT_METRIC_VALUE, ROOT_DIR


class OSResourceUsage:
    """
        Class that evaluates and returns resource usage from
        operating system. This will be required to monitor
        model performance.
    """

    def __init__(self):
        self._psutil = psutil
        self._GPUtil = GPUtil
        self._cpu_process = psutil.Process()

    def evaluate_cpu_metrics(self) -> list:
        """
            Calculate generic CPU metrics
        Returns:
            metrics(list)
        """
        total_cores = psutil.cpu_count()
        self._cpu_process.cpu_percent()
        system_cpu_usage_pc = self._cpu_process.cpu_percent(interval=MIN_INTERVAL)
        return [total_cores, system_cpu_usage_pc]

    def evaluate_cpu_mem_metrics(self) -> list:
        """
            Calculate CPU memory metrics
        Returns:
            metrics(list)
        """
        mem_usage_bytes = self._cpu_process.memory_info().rss
        mem_total_bytes = psutil.virtual_memory().total
        return [mem_usage_bytes, mem_total_bytes]

    def evaluate_gpu_metrics(self) -> list:
        """
            Calculate generic GPU metrics
        Returns:
            metrics(list)
        """
        total_gpu = len(self._GPUtil.getGPUs())
        return [total_gpu]

    def evaluate_gpu_mem_metrics(self) -> list:
        """
            Calculate GPU memory metrics
        Returns:
            metrics(list)
        """
        all_gpu = self._GPUtil.getGPUs()
        total_gpu = len(all_gpu)
        if total_gpu >= 1:
            all_gpu.sort(key=lambda g: g.memoryUtil)
            gpu_mem_usage_bytes = all_gpu[LAST_ELEMENT_INDEX].memoryUsed / CONV_FACTOR
            gpu_mem_total_bytes = all_gpu[LAST_ELEMENT_INDEX].memoryTotal / CONV_FACTOR
        else:
            gpu_mem_usage_bytes = DEFAULT_METRIC_VALUE
            gpu_mem_total_bytes = DEFAULT_METRIC_VALUE
        return [gpu_mem_usage_bytes, gpu_mem_total_bytes]

    def evaluate_root_disk_metrics(self) -> list:
        """
            Calculate root disk memory metrics
        Returns:
            metrics(list)
        """
        root_disk_usage_bytes = self._psutil.disk_usage(ROOT_DIR).used
        root_disk_total_bytes = self._psutil.disk_usage(ROOT_DIR).total
        return [root_disk_usage_bytes, root_disk_total_bytes]

    def evaluate_partition_disk_metrics(self) -> list:
        """
            Calculate partition disk metrics
        Returns:
            metrics(list)
        """
        partitions = self._psutil.disk_partitions()
        partitions.sort(key=lambda p: self._psutil.disk_usage(p.mountpoint).used)
        top_partition_disk_usage_bytes = \
            self._psutil.disk_usage(partitions[LAST_ELEMENT_INDEX].mountpoint).used
        top_partition_disk_total_bytes = \
            self._psutil.disk_usage(partitions[LAST_ELEMENT_INDEX].mountpoint).total
        return [top_partition_disk_usage_bytes, top_partition_disk_total_bytes]

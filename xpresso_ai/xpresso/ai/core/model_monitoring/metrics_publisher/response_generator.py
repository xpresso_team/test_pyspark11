"""
    Class implementation of response metrics generator
"""

__all__ = ["ResponseGenerator"]
__author__ = ["Shlok Chaudhari"]


from datetime import datetime
from xpresso.ai.core.model_monitoring.metrics_publisher.metric_type import Response
from xpresso.ai.core.model_monitoring.metrics_publisher.abstract_metric_generator import \
    AbstractMetricGenerator
from xpresso.ai.core.commons.exceptions.xpr_exceptions import InvalidModelMonitoringData
from xpresso.ai.core.commons.utils.constants import METRIC_TYPE_RESPONSE, MODEL_ID, MODEL_REQUEST_ID, \
    TYPE_KEY, TIME


class ResponseGenerator(AbstractMetricGenerator):
    """
        This class formulates response metrics for model monitoring
    """

    def __init__(self, model_id: str, request_id: str, response_data: dict):
        super().__init__(model_id, METRIC_TYPE_RESPONSE)
        self._request_id = request_id
        self._response_data = response_data
        self._response_schema = Response.get()
        self._generate_metrics()

    def _generate_metrics(self):
        """
            Generates response metrics to be sent for monitoring
        Returns:
            metrics(dict)
        """
        self._response_data.update({
            MODEL_ID: self._model_id,
            MODEL_REQUEST_ID: self._request_id,
            TYPE_KEY: self._metric_type,
            TIME: datetime.utcnow().isoformat()
        })
        if not set(list(self._response_data.keys())) == set(self._response_schema):
            raise InvalidModelMonitoringData("Response table schema is not being followed")

    def get_metrics(self):
        """
            Get response metrics in publishable schema
        Returns:
            metrics(dict)
        """
        return self._response_data

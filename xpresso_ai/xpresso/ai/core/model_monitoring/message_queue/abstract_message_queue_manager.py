from abc import ABCMeta, abstractmethod

class AbstractMessageQueueManager(metaclass=ABCMeta):
    """
    Interface for Messqge Queue managers
    """
    def __init__(self):
        """ constructor for Message Queue manager interface """
        pass

    @abstractmethod
    def publish(self,message=None):
        """Publishes the data to the message queue"""
        pass

    @abstractmethod
    def consume(self,method=None):
        """Dumps the data into the database"""
        pass
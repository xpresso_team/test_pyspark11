__all__ = ['MessageQueueFactory']
__author__ = 'Srijan Sharma'

from xpresso.ai.core.commons.utils.xpr_config_parser \
    import XprConfigParser
from xpresso.ai.core.model_monitoring.message_queue.rabbit_mq_manager import RabbitMQManager
from xpresso.ai.core.commons.utils.constants import MODEL_MONITORING_SECTION, TYPE_KEY, \
    MESSAGE_QUEUE_SECTION
from xpresso.ai.core.commons.exceptions.xpr_exceptions import InvalidMessageQueueException


class MessageQueueFactory:
    """
    factory class for MessageQueueManager
    """
    RABBIT_MQ_TYPE = "rabbitmq"

    def __init__(self):
        self.xpr_config = XprConfigParser()

    def create_manager(self):
        """
        Creates an appropriate MessageQueueFactory.

        Returns:
            returns MessageQueueFactory manager class reference
        """
        message_queue_config = self.xpr_config[MODEL_MONITORING_SECTION][MESSAGE_QUEUE_SECTION]
        if message_queue_config[TYPE_KEY] == self.RABBIT_MQ_TYPE:
            return RabbitMQManager()
        else:
            raise InvalidMessageQueueException()

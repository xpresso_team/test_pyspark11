"""
    Abstract requests client for Model Monitoring
"""

__all__ = ["AbstractRequestClient"]
__author__ = ["Shlok Chaudhari"]


from abc import ABCMeta, abstractmethod
from xpresso.ai.core.logging.xpr_log import XprLogger


class AbstractRequestClient(metaclass=ABCMeta):
    """
        This is a base class for all the requests being sent
        to Model Monitoring API server
    """

    _logger = XprLogger()

    def __init__(self):
        pass

    @abstractmethod
    def send(self, *args, **kwargs):
        """ Abstract method for sending request """

    @abstractmethod
    def _process_response(self, *args, **kwargs):
        """ Abstract method for processing the response """

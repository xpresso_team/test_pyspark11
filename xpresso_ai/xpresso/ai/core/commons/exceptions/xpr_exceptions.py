""" all xpresso excptions """

from xpresso.ai.core.commons.utils import error_codes
from xpresso.ai.core.commons.utils.constants import follow_user_manual_message, \
    follow_project_manual_message, user_manual_link


class XprExceptions(Exception):
    """ General Xpresso exception occurred """

    def __init__(self, message: str = None):
        self.message = message
        self.error_code = 777

    def __str__(self):
        """
        gets a string representation of this exception
        :return: string representation of exception
        """
        return self.message


class UserNotFoundException(XprExceptions):
    """
    class for exception thrown when the requested user is not found in the
    persistence.
    """

    def __init__(self, message: str = "User not found"):
        self.message = message
        self.error_code = error_codes.user_not_found


class DuplicateUserException(XprExceptions):
    """
    occurs when a request comes to create a new user whose uid already exists
    """

    def __init__(self, message: str = "This uid is already taken."):
        self.message = message
        self.error_code = error_codes.username_already_exists


class DeactivatedUserException(XprExceptions):
    """
    class for exception thrown when the requested user is not found in the
    persistence.
    """

    def __init__(self, message: str = "User already deactivated"):
        self.message = message
        self.error_code = error_codes.user_already_deactivated


class LogoutFailedException(XprExceptions):
    """
    class for exception thrown when logout request fails.
    """


class WrongPasswordException(XprExceptions):
    """
    class for exception thrown when the authentication fails
    """

    def __init__(self, message: str = "The password that you have "
                                      "entered is incorrect."):
        self.message = message
        self.error_code = error_codes.wrong_pwd


class AuthenticationFailedException(XprExceptions):
    """
    class for exception thrown when the authentication fails
    """

    def __init__(self, message: str = "Authentication Failed"):
        self.message = message
        self.error_code = error_codes.auth_failed


class AlreadyLoggedInException(XprExceptions):
    def __init__(self, message: str = "Already Logged In"):
        self.message = message
        self.error_code = error_codes.already_logged_in


class UnsuccessfulConnectionException(XprExceptions):
    """
    Class to define an exception thrown when a database connection was
    unsuccessful
    """

    def __init__(self, message: str = "Database Connection Issue"):
        self.message = message
        self.error_code = error_codes.unsuccessful_connection


class UnsuccessfulOperationException(XprExceptions):
    """
    Class to define an exception thrown when a database operation was
    unsuccessful. Usually indicates a duplicate key error.
    """

    def __init__(self, message: str = "Unsuccessful Operation"):
        self.message = message
        self.error_code = error_codes.unsuccessful_operation


class ClusterNameBlankException(XprExceptions):
    """
    Thrown when user provides blank cluster name while registering
    """

    def __init__(self, message: str = "Cluster name is invalid"):
        self.message = message
        self.error_code = error_codes.cluster_name_blank


class ClusterAlreadyExistsException(XprExceptions):
    """
    class for exception thrown when the cluster to be registered already exists
    """

    def __init__(self, message: str = "Cluster already exists"):
        self.message = message
        self.error_code = error_codes.cluster_already_exists


class ClusterNotFoundException(XprExceptions):
    """
    class for exception thrown when the cluster to be deleted doesn't exist
    """

    def __init__(self, message: str = "Cluster not found"):
        self.message = message
        self.error_code = error_codes.cluster_not_found


class IncompleteClusterInfoException(XprExceptions):
    """
    class for exception thrown when the cluster information isn't sufficient
    """

    def __init__(self, message: str = "Cluster info incomplete"):
        self.message = message
        self.error_code = error_codes.incomplete_cluster_info


class BuildRequestFailedException(XprExceptions):
    """
    class for exception thrown when project build fails.
    """

    def __init__(self, message: str = "Project build failed"):
        self.message = message
        self.error_code = error_codes.project_build_failed


class DeployRequestFailedException(XprExceptions):
    """
    class for exception thrown when project deployment fails.
    """

    def __init__(self, message: str = "Deploy Request Failed"):
        self.message = message
        self.error_code = error_codes.project_deployment_failed


class BlankFieldException(XprExceptions):
    """
    Class to define an exception thrown when a field is 
    blank in an object input
    """

    def __init__(self, message: str = "Blank Field Provided"):
        self.message = message
        self.error_code = error_codes.blank_field_error


class MissingFieldException(XprExceptions):
    """
    Class to define an exception thrown when a field is missing from an object
    input
    """

    def __init__(self, message: str = "One or more field is missing"):
        self.message = message
        self.error_code = error_codes.missing_field_error


class ExpiredTokenException(XprExceptions):
    """
    Class to define an exception thrown when the authentication token supplied
    has expired
    """

    def __init__(self, message: str = "Your login session has expired. "
                                      "Please re-login."):
        self.message = message
        self.error_code = error_codes.expired_token


class IncorrectTokenException(XprExceptions):
    """
    Class to define an exception thrown when the authentication token supplied
    is wrong
    """

    def __init__(self, message: str = "Your existing session is invalid."
                                      " Please re-login."):
        self.message = message
        self.error_code = error_codes.wrong_token


class TokenNotSpecifiedException(XprExceptions):
    """
    Class to define an exception thrown when an authentication token
    is not specified
    """

    def __init__(self, message: str = "Unable to find a token for this session."
                                      " Please re-login."):
        self.message = message
        self.error_code = error_codes.wrong_token


class PermissionDeniedException(XprExceptions):
    """
    Class to define an exception thrown when the user is denied permission for
    the specified action
    """

    def __init__(self, message: str = "Permission Denied! You don't have"
                                      " access to run this method"):
        self.message = message
        self.error_code = error_codes.permission_denied


class InvalidValueException(XprExceptions):
    """
    Class to define an exception thrown when a the value of a field is invalid
    in an object input
    """

    def __init__(self, message: str = "Invalid value provided"):
        self.message = message
        self.error_code = error_codes.invalid_value_error


class InvalidUserIDException(XprExceptions):
    """
    Class to define an exception thrown when the user id is either not
    specified or blank
    """

    def __init__(self, message: str = f"Invalid  user id. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.empty_uid


class InvalidNodeException(XprExceptions):
    def __init__(self, message: str = "Invalid  user node"):
        self.message = message
        self.error_code = error_codes.invalid_node_data


class UnexpectedNodeException(XprExceptions):
    """
        This exception is called when the host key for the api_server address
        provided is unavailable
    """

    def __init__(self, message: str = "Node unavailable"):
        self.message = message
        self.error_code = error_codes.node_not_found


class BranchNotSpecifiedException(XprExceptions):
    """
    Thrown when no branch is specified while building a project
    """

    def __init__(self, message: str = "Branch not specified"):
        self.message = message
        self.error_code = error_codes.branch_not_specified


class InvalidBuildVersionException(XprExceptions):
    """
    Thrown when build version is not specified while deploying a project
    """

    def __init__(self, message: str = "Invalid build version"):
        self.message = message
        self.error_code = error_codes.invalid_build_version


class IncompleteProjectInfoException(XprExceptions):
    """
    Thrown when user has provided insufficient info while building/deploying
    """

    def __init__(self, message: str = f"Incomplete project info. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.incomplete_project_information


class ComponentsSpecifiedIncorrectlyException(XprExceptions):
    """
    Thrown when components to be built/deployed are specified incorrectly
    """

    def __init__(self, message: str = f"Component specified incorrectly. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.components_specified_incorrectly


class PipelinesSpecifiedIncorrectlyException(XprExceptions):
    """
    Thrown when pipelines to be built/deployed are specified incorrectly
    """

    def __init__(self, message: str = f"Pipeline specified incorrectly. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.components_specified_incorrectly


class ServiceMeshSpecifiedIncorrecltyException(XprExceptions):
    """
    Thrown when service mesh name specified incorrectly
    """

    def __init__(self, message: str = f"Pipeline specified incorrectly. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.components_specified_incorrectly


class CurrentlyNotDeployedException(XprExceptions):
    """
    Thrown when user attempts to undeploy a project which isn't currently
    deployed
    """

    def __init__(self, message: str = "Currently not deployed"):
        self.message = message
        self.error_code = error_codes.currently_not_deployed


class ClusterRequestFailedException(XprExceptions):
    """
    class for exception thrown when the requested cluster is
    not found in the db.
    """


class HTTPRequestFailedException(XprExceptions):
    """
    class for exception thrown when HTTP request fails
    """

    def __init__(self, message: str = "HTTP request failed"):
        self.message = message
        self.error_code = error_codes.http_request_failed


class HTTPInvalidRequestException(XprExceptions):
    """
    class for exception thrown when HTTP request is invalid
    """

    def __init__(self, message: str = "Invalid Http request"):
        self.message = message
        self.error_code = error_codes.invalid_http_request


class CLICommandFailedException(XprExceptions):
    """
    throws when the cli command fails
    """

    def __init__(self, message: str = "CLI execution failed."):
        self.message = message
        self.error_code = error_codes.client_error


class ControllerClientResponseException(XprExceptions):
    """
    throws when the controller client fails
    """

    def __init__(self, message: str, error_code: int):
        self.message = message
        self.error_code = error_code

    def __str__(self):
        """
        gets a string representation of this exception
        return:
            string representation of exception
        """
        return "Code {}: {}".format(self.error_code, self.message)


class ProjectFormatException(XprExceptions):
    """
    This exception is called when the project format is not valid
    """

    def __init__(self, message: str = "Project format is invalid. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_project_format


class ProjectConfigException(XprExceptions):
    """
    This exception is called when the project config file is not
    loaded correctly
    """

    def __init__(self,
                 message: str = "Project configuration is invalid. "
                                f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.internal_config_error


class ProjectFieldsException(XprExceptions):
    """
    This exception is called when the input fields provided are not
    following the format
    """

    def __init__(self, message: str = "Invalid project field format. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_project_field_format


class DuplicateComponentException(XprExceptions):
    """
    This exception is called when there is request for a duplicate component
    """

    def __init__(self, message: str = "Component already exists."):
        self.message = message
        self.error_code = error_codes.component_already_exists


class ComponentFieldException(XprExceptions):
    """
    This exception is called when there are bugs with component field's
    format
    """

    def __init__(self, message: str = "Unkown component specified."
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.unknown_component_key


class ProjectOwnerException(XprExceptions):
    """
    This exception is called if the owner information is incorrect
    """

    def __init__(self, message: str = "Invalid project owner"):
        self.message = message
        self.error_code = error_codes.invalid_owner_information


class BadHostkeyException(XprExceptions):
    """
    This exception is called when the host key for the api_server address
    provided is unavailable
    """

    def __init__(self, message: str = "Node not found"):
        self.message = message
        self.error_code = error_codes.node_not_found


class ProjectNotFoundException(XprExceptions):
    """
    Thrown when project to be deployed/built is not found
    """

    def __init__(self, message: str = "Project not found"):
        self.message = message
        self.error_code = error_codes.project_not_found


class ServiceCreationFailedException(XprExceptions):
    """
    Thrown when kubernetes api fails to create a service
    """

    def __init__(self, message: str = "Service creation failed"):
        self.message = message
        self.error_code = error_codes.service_creation_failed


class PortPatchingAttemptedException(XprExceptions):
    """
    Thrown when user attempts editing a deployed port's specs
    """

    def __init__(self, message: str = "Port patching attempted"):
        self.message = message
        self.error_code = error_codes.port_patching_attempted


class NamespaceCreationFailedException(XprExceptions):
    """
    Thrown when kubernetes api fails to create a namespace
    """

    def __init__(self, message: str = "Namespace creation failed"):
        self.message = message
        self.error_code = error_codes.namespace_creation_failed


class DockerSecretCreationFailed(XprExceptions):
    """
    Thrown when kubernetes api fails to create docker secret
    """

    def __init__(self, message: str = "Docker secret creation failed"):
        self.message = message
        self.error_code = error_codes.docker_secret_creation_failed

class PodDetailsFetchFailed(XprExceptions):
    """
    Thrown when kubernetes api fails to create docker secret
    """

    def __init__(self, message: str = "Could not fetch details of the pods"):
        self.message = message
        self.error_code = error_codes.kubernetes_error

class JobCreationFailedException(XprExceptions):
    """
    Thrown when kubernetes api fails to create a job
    """

    def __init__(self, message: str = "Job creation failed"):
        self.message = message
        self.error_code = error_codes.job_creation_failed


class CronjobCreationFailedException(XprExceptions):
    """
    Thrown when kubernetes api fails to create a cronjob
    """

    def __init__(self, message: str = "Cronjob creation failed"):
        self.message = message
        self.error_code = error_codes.cronjob_creation_failed


class ProjectDeploymentFailedException(XprExceptions):
    """
    Thrown when a project fails to deploy on Kubernetes
    """

    def __init__(self, message: str = "Project deployment failed"):
        self.message = message
        self.error_code = error_codes.project_deployment_failed


class ProjectUndeploymentFailedException(XprExceptions):
    """
    Thrown when a project fails to undeploy from Kubernetes
    """

    def __init__(self, message: str = "Project undeployment failed"):
        self.message = message
        self.error_code = error_codes.project_undeployment_failed


class InvalidJobTypeException(XprExceptions):
    """
    Thrown when user specifies an invalid or empty job type
    """

    def __init__(self, message: str = "Invalid job type. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_job_type


class InvalidJobCommandsException(XprExceptions):
    """
    Thrown when user specifies invalid or empty job commands
    """

    def __init__(self, message: str = "Invalid job command. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_job_commands


class ProjectDeveloperException(XprExceptions):
    """
    This exception is called if the developers information is incorrect
    """

    def __init__(self, message: str = "Invalid developer info. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_developer_information


class ProjectPipelinesException(XprExceptions):
    """
    This exception is called when the pipelines info provided is incorrect
    """

    def __init__(self, message: str = "One or more pipeline default "
                                      "keys were specified incorrectly. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.incorrect_pipelines_information


class ProjectSetupException(XprExceptions):
    """
    This exception is called while facing any issue in the project setup
    """

    def __init__(self, message: str = "Project Setup Failed"):
        self.message = message
        self.error_code = error_codes.project_setup_failed


class SkeletonCodeException(XprExceptions):
    """
    This exception is called if there is any issue in setting skeleton repo
    """

    def __init__(self, message: str = "Skeleton repo creation failed"):
        self.message = message
        self.error_code = error_codes.skeleton_repo_creation_failed


class BitbucketCloneException(XprExceptions):
    """
    This exception is called when there is any issue in cloning a repo
    """

    def __init__(self, message: str = "Repo clone failed"):
        self.message = message
        self.error_code = error_codes.repo_clone_failed


class ProjectDeactivationException(XprExceptions):
    """
    This exception occurs if there is any error while deactivating project
    """

    def __init__(self, message: str = "Project deactivation failed"):
        self.message = message
        self.error_code = error_codes.project_deactivation_failed


class NoProjectException(XprExceptions):
    """
    This exception is called if there is any error in modifying project
    """

    def __init__(self, message: str = "Project not created"):
        self.message = message
        self.error_code = error_codes.project_not_created


class InvalidCronScheduleException(XprExceptions):
    """
    Thrown when user provides an invalid cron job schedule
    """

    def __init__(self, message: str = "Invalid cron schedule. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_cron_schedule


class DeploymentCreationFailedException(XprExceptions):
    """
    Thrown when kubernetes api fails to create a deployment
    """

    def __init__(self, message: str = "Deployment creation failed"):
        self.message = message
        self.error_code = error_codes.deployment_creation_failed


class IncompleteUserInfoException(XprExceptions):
    """
    This exception is called when provided user info is not complete
    """

    def __init__(self, message: str = "Incomplete user information. "
                                      f"{follow_user_manual_message}"):
        self.message = message
        self.error_code = error_codes.incomplete_user_information


class PasswordStrengthException(XprExceptions):
    """
    This exception is called when the password strength is not enough
    """

    def __init__(self, message: str = f"Password is weak. Please refer to "
                                      f"{user_manual_link} for password rules"):
        self.message = message
        self.error_code = error_codes.password_not_valid


class IncompleteProvisionInfoException(XprExceptions):
    """
    This exception is called when the provision info is incomplete
    """

    def __init__(self, message: str = "Incomplete provision info"):
        self.message = message
        self.error_code = error_codes.incomplete_provision_information


class InvalidProvisionInfoException(XprExceptions):
    """
    This exception is called when the provision info is invalid
    """

    def __init__(self, message: str = "Invalid provision info"):
        self.message = message
        self.error_code = error_codes.invalid_provision_information


class NodeNotFoundException(XprExceptions):
    """
    This exception is called when the node is not found
    """

    def __init__(self, message: str = "Node not found"):
        self.message = message
        self.error_code = error_codes.node_not_found


class NodeReProvisionException(XprExceptions):
    """
    This exception is called when the node is already provisioned
    """

    def __init__(self, message: str = "Node already provisioned"):
        self.message = message
        self.error_code = error_codes.node_already_provisioned


class InvalidMasterException(XprExceptions):
    """
    This exception is called when the master node info is invalid
    """

    def __init__(self, message: str = "Invalid master node"):
        self.message = message
        self.error_code = error_codes.invalid_master_node


class MasterNotProvisionedException(XprExceptions):
    """
    This exception is called when the master node is not provisioned
    """

    def __init__(self, message: str = "Master not provisioned"):
        self.message = message
        self.error_code = error_codes.master_not_provisioned


class ProvisionKubernetesException(XprExceptions):
    """
    This exception is called when the node provision is failed
    because of kubernetes error
    """

    def __init__(self, message: str = "Kubernetes error"):
        self.message = message
        self.error_code = error_codes.kubernetes_error


class NodeDeactivatedException(XprExceptions):
    """
    This exception is called when the node is already deactivated
    """

    def __init__(self, message: str = "Node already deactivated"):
        self.message = message
        self.error_code = error_codes.node_already_deactivated


class IncompleteNodeInfoException(XprExceptions):
    """
    This exception is called when the Node information is incomplete
    """

    def __init__(self, message: str = "Incomplet node information"):
        self.message = message
        self.error_code = error_codes.incomplete_node_information


class UnProvisionedNodeException(XprExceptions):
    """
    This exception is called when the node is not provisioned
    """

    def __init__(self, message: str = "Node not provisioned"):
        self.message = message
        self.error_code = error_codes.node_not_provisioned


class NodeTypeException(XprExceptions):
    """
    This exception is called when the node type is invalid
    """

    def __init__(self, message: str = "Invalid node type"):
        self.message = message
        self.error_code = error_codes.invalid_node_type


class NodeAlreadyAssignedException(XprExceptions):
    """
    This exception is called when the node is already assigned
    """

    def __init__(self, message: str = "Node already assigned"):
        self.message = message
        self.error_code = error_codes.node_already_assigned


class NodeAssignException(XprExceptions):
    """
    This exception is called when the node assignation failed
    """

    def __init__(self, message: str = "Node assignment failed"):
        self.message = message
        self.error_code = error_codes.node_assign_failed


class CallDeactivateNodeException(XprExceptions):
    """
    This exception is called when the node assignation failed
    """

    def __init__(self, message: str = None):
        self.message = message
        self.error_code = error_codes.call_deactivate_node


class FileNotFoundException(XprExceptions):
    """
    This exception is called when the file is not present at the path
    """

    def __init__(self, message: str = None):
        self.message = message
        self.error_code = error_codes.file_not_found


class JsonLoadError(XprExceptions):
    """
    This exception is called when the file is not present at the path
    """

    def __init__(self, message: str = None):
        self.message = message
        self.error_code = error_codes.json_load_error


class NodeDeletionKubernetesException(XprExceptions):
    """
    This exception is called when the node deletion failed
    because of kubernetes error
    """

    def __init__(self, message: str = "Node Deletion failed"):
        self.message = message
        self.error_code = error_codes.kubernetes_error


class IllegalModificationException(XprExceptions):
    """
    Class to define an exception thrown when a field is attempted to be
    modified illegally
    """

    def __init__(self, message: str = "Illegal modification"):
        self.message = message
        self.error_code = error_codes.cannot_modify_password


class InvalidPasswordException(XprExceptions):
    """
    Modification is not correct
    """

    def __init__(self, message: str = "Invalid password"):
        self.message = message
        self.error_code = error_codes.password_not_valid


class IncorrectDeclarativeJSONDefinitionException(XprExceptions):
    """
    Thrown when the provided declarative JSON for Kubeflow pipeline is
    incorrectly defined
    """

    def __init__(self,
                 message: str = "Declarative JSON defined incorrectly. "
                                f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.declarative_json_incorrect


class ReferenceNotFoundException(XprExceptions):
    """
    Thrown when an object being referred is not found in declarative json
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.reference_not_found


class PipelineNotFoundException(XprExceptions):
    """
    Thrown when the pipeline in question is not found in the database
    """

    def __init__(self, message: str = "Pipeline doesn't exist."):
        self.message = message
        self.error_code = error_codes.pipeline_not_found


class KubeflowDashboardPortFetchException(XprExceptions):
    """
    Thrown when kubeflow dashboard port patching fails
    """

    def __init__(self, message: str = "Kubeflow Dashboard port "
                                      "fetching failed."):
        self.message = message
        self.error_code = error_codes.kubeflow_dashboard_port_fetching_failed


class PipelineUploadFailedException(XprExceptions):
    """
    thrown when pipeline upload via API fails
    """

    def __init__(self, message: str = "Pipeline upload failed."):
        self.message = message
        self.error_code = error_codes.pipeline_upload_failed


class InvalidDatatypeException(XprExceptions):
    """
    Invalid Datatype Provided
    """

    def __init__(self, message: str = "Invalid Datatype Passed. "
                                      f"{follow_project_manual_message}"):
        self.message = message


class ExceedsDataSize(XprExceptions):
    """
    Size of data is more than expected
    """

    def __init__(self, message: str = "Exceeds max data size. "):
        self.message = message
        self.error_code = error_codes.data_size_exceeded


class EmailException(XprExceptions):
    """
    Unable to send the email notification
    """

    def __init__(self, message: str = "Unable to send"):
        self.message = message
        self.error_code = error_codes.password_not_valid


class APIGatewayExceptions(XprExceptions):
    """
    Modification is not correct
    """

    def __init__(self, message: str = "Gateway is not working as expected"):
        self.message = message
        self.error_code = error_codes.gateway_connection_error


class APIGatewayDuplicateExceptions(XprExceptions):
    """
    Same services or route exists
    """

    def __init__(self, message: str = "Duplicate entry in the gateway"):
        self.message = message
        self.error_code = error_codes.gateway_connection_error


class InvalidArgumentException(XprExceptions):
    """
    This exception is thrown when the provided Arguments does not contain
    expected data or is of invalid type
    """


class InvalidConfigException(XprExceptions):
    """
    This exception is thrown when the config file is invalid
    """

    def __init__(self, message: str = "configuration is invalid."):
        self.message = message
        self.error_code = error_codes.internal_config_error


class PasswordEncryptionError(XprExceptions):
    """
    This exception is thrown when the password cannot be encrypted or decrypted
    """

    def __init__(self, message: str = "Encrypt the passwords before decryption"):
        self.message = message
        self.error_code = error_codes.password_encryption_error


class FileEncryptionError(XprExceptions):
    """
    This exception is thrown when the config file cannot be encrypted or decrypted
    """

    def __init__(self, message: str = "Encrypt the file before decrypting"):
        self.message = message
        self.error_code = error_codes.file_encryption_error


class CommandExecutionFailedException(XprExceptions):
    """
    This exception is thrown when command execution failed
    """

    def __init__(self, command_name: str):
        """
        command_name is the name/kind of the command that failed
        """
        self.error_code = error_codes.command_execution_failure
        self.message = \
            f"Command execution failure. Unable to run the" \
            f" {command_name} command successfully."


class BundleFailedException(XprExceptions):
    """
    This exception is thrown when package is not exited successfully while
    performing a the task
    """

    def __init__(self, message: str = "Bundle installation failed"):
        self.message = message
        self.error_code = error_codes.bundle_failed


class BundleNotSupportedException(XprExceptions):
    """
    This exception is thrown when package is not exited successfully while
    performing a the task
    """

    def __init__(self, message: str = "Bundle not supported"):
        self.message = message
        self.error_code = error_codes.bundle_unsupported


class SerializationFailedException(XprExceptions):
    """ Raised when serialization failed for an object"""

    def __init__(self, message: str = "Serialization Failed"):
        self.message = message
        self.error_code = error_codes.serialization_failed


class DeserializationFailedException(XprExceptions):
    """ Raised when deserialization failed for an object"""

    def __init__(self, message: str = "Deserialization Failed"):
        self.message = message
        self.error_code = error_codes.deserialization_failed


class InvalidEnvironmentException(XprExceptions):
    """
    invalid environment specified for project deployment
    """

    def __init__(self, message: str = "Invalid environment specified"):
        self.message = message
        self.error_code = error_codes.invalid_environment_error


class NoClustersPresentException(XprExceptions):
    """
    no clusters present for environment allocation
    """

    def __init__(self,
                 message: str = "No clusters present for environment "
                                "allocation"):
        self.message = message
        self.error_code = error_codes.no_clusters_present_error


class IncorrectDeploymentException(XprExceptions):
    """
    attempt to deploy project to higher environment before deploying to all
    possible lower environments
    """

    def __init__(self,
                 message: str = "Attempt to deploy project to higher "
                                "environment before deploying to all "
                                "possible lower environments"):
        self.message = message
        self.error_code = error_codes.incorrect_deployment_error


class RepoNotProvidedException(XprExceptions):
    """
    Repo Name is not provided
    """

    def __init__(self, message: str = "Repo name not provided"):
        self.message = message
        self.error_code = error_codes.pachyderm_repo_not_provided


class DatasetInfoException(XprExceptions):
    """
    Dataset info provided is incomplete or invalid
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.dataset_info_error


class DatasetPathException(XprExceptions):
    """
    Dataset path is invalid or not found
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.dataset_path_invalid


class BranchInfoException(XprExceptions):
    """
    occurs when branch info is invalid/incomplete
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.pachyderm_branch_info_error


class BranchNotFoundException(XprExceptions):
    """
    occurs when branch info is invalid/incomplete
    """

    def __init__(self, message="No branch found with provided information"):
        self.message = message
        self.error_code = error_codes.pachyderm_branch_info_error



class PachydermFieldsNameException(XprExceptions):
    """
    occurs when name of any key fields of pachyderm does not follow pattern
    """

    def __init__(self,
                 message="Name of repo, branch and automl can only be"
                         "string of alphanumeric characters, underscores"
                         "or dashes"):
        self.message = message
        self.error_code = error_codes.pachyderm_field_name_error


class PachydermOperationException(XprExceptions):
    """
    occurs when any pachyderm operation fails
    """

    def __init__(self, message="Operation failed because of an issue"
                               " with Data Versioning platform."):
        self.message = message
        self.error_code = error_codes.pachyderm_operation_error


class LocalFilePathException(XprExceptions):
    """
    occurs when there is any exception with local path
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.local_path_exception


class PachydermEnvironmentException(XprExceptions):
    """
    occurs when there is an exception with environment setup for pachyderm
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.pachyderm_env_exception


class PachydermSetupException(XprExceptions):
    """
    occurs if any exception occurs during pachyderm setup
    """

    def __init__(self, message):
        self.message = message
        self.error_code = error_codes.pachyderm_setup_error


class PachydermRemovalException(XprExceptions):
    """
    occurs if any error is thrown while removing/uninstalling pachyderm
    """

    def __init__(self, message: str = "Pachyderm uninstallation failed"):
        self.message = message
        self.error_code = error_codes.pachyderm_uninstallation_error


class HelmSetupException(XprExceptions):
    """
    occurs if any errors occur while setting helm
    """

    def __init__(self, message: str = "Helm setup failed"):
        self.message = message
        self.error_code = error_codes.helm_setup_failed


class HelmRemovalException(XprExceptions):
    """
    occurs if any error occurs while removing helm
    """

    def __init__(self, message: str = "Helm removal failed"):
        self.message = message
        self.error_code = error_codes.helm_removal_failed


class MinioSetupException(XprExceptions):
    """
    occurs if any errors occur while setting helm
    """

    def __init__(self, message: str = "Minio setup failed"):
        self.message = message
        self.error_code = error_codes.minio_setup_failed


class MinioRemovalException(XprExceptions):
    """
    occurs if any error occurs while removing minio
    """

    def __init__(self, message: str = "Minio removal failed"):
        self.message = message
        self.error_code = error_codes.minio_removal_failed


class EmailFormatException(XprExceptions):
    """
    occurs if the email provided is in invalid format
    """

    def __init__(self, message: str = f"Invalid format for email address. "
                                      f"{follow_user_manual_message}"):
        self.message = message
        self.error_code = error_codes.email_format_exception


class EmailDomainException(XprExceptions):
    """
    occurs if the email provided is in invalid format
    """

    def __init__(self, message: str = "Invalid domain name in email address. "
                                      + follow_user_manual_message):
        self.message = message
        self.error_code = error_codes.email_domain_exception


class PrimaryKeyException(XprExceptions):
    """
    occurs if primary key violation occurs during insert operation to db
    """

    def __init__(self,
                 message: str = "Insert failed due to primary key violation"):
        self.message = message
        self.error_code = error_codes.primary_key_violation


class UidFormatException(XprExceptions):
    """
    occurs when uid format entered is invalid
    """

    def __init__(self, message: str = f"uid format is invalid."
                                      f"{follow_user_manual_message}"):
        self.message = message
        self.error_code = error_codes.uid_format_invalid


class ProjectNameException(XprExceptions):
    """
    occurs if the project name is invalid
    """

    def __init__(self, message: str = ""):
        project_name_format = f"Project name format is invalid. " \
                              f"{follow_project_manual_message}"
        self.message = message or project_name_format
        self.error_code = error_codes.invalid_project_name


class ProjectAlredyExistException(XprExceptions):
    """
    occurs if the project name is invalid
    """

    def __init__(self, message: str = ""):
        project_name_format = f"Project with the same name already exist."
        self.message = message or project_name_format
        self.error_code = error_codes.project_already_exist


class MultivariatePlotException(XprExceptions):
    """
    occurs if unable to convert the correlation matrix to a format as 
    required by visualisation module.
    """

    def __init__(self, message: str = "Unable to convert the correlation "
                                      "matrix to a suitable format as "
                                      "required by visualization module"):
        self.message = message
        self.error_code = error_codes.heatmap_format_convert_exception


class PachydermConnectionException(XprExceptions):
    """
    occurs if the connection to pachyderm cluster fails
    """

    def __init__(self,
                 message: str = "Connection to pachyderm api_server failed"):
        self.message = message
        self.error_code = error_codes.pachyderm_connection_error


class PachydermServerException(XprExceptions):
    """
    occurs if there are any issues with pachyderm api_server
    """

    def __init__(self, message: str = "Pachyderm api_server error"):
        self.message = message
        self.error_code = error_codes.pachyderm_server_error


class RepoInfoException(XprExceptions):
    """
    occurs if the repo info is invalid
    """

    def __init__(self, message: str = "Invalid repo information"):
        self.message = message
        self.error_code = error_codes.invalid_pachyderm_repo_info


class PersistentVolumeSizeException(XprExceptions):
    """
    occurs if the pv size requested is more than the upper limit
    """

    def __init__(self, message: str = "Persistent Volume size requested is "
                                      "above the upper limit."):
        self.message = message
        self.error_code = error_codes.pv_upper_limit_error


class RepoPermissionException(XprExceptions):
    """
    occurs in case of a invalid access request to a repo
    """

    def __init__(self, message: str = "You do not have access to this repo"):
        self.message = message
        self.error_code = error_codes.repo_permission_error


class CreateBranchException(XprExceptions):
    """
    occurs if create branch operation is invalid
    """

    def __init__(self, message: str = "Unable to create branch"):
        self.message = message
        self.error_code = error_codes.pachyderm_branch_info_error


class NFSSetupException(XprExceptions):
    """
    Occurs when setting up project or user in the NFS
    """

    def __init__(self, message: str = "Project setup failed on NFS. "
                                      "Permission issue"):
        self.message = message
        self.error_code = error_codes.nfs_setup_failed


class PrimaryRoleException(XprExceptions):
    """
    Occurs if there is any issue with primary role
    """

    def __init__(self, message: str = f"primaryRole is invalid. "
                                      f"{follow_user_manual_message}"):
        self.message = message
        self.error_code = error_codes.primary_role_invalid


class LdapUserAdditionException(XprExceptions):
    """
    Occurs if there is any issue with ldap manager
    """

    def __init__(self, message: str = "Failed to add user to ldap server"):
        self.message = message
        self.error_code = error_codes.ldap_user_addition_failed


class UserFieldsException(XprExceptions):
    """
    Occurs if there is any issue with fields in user info
    """

    def __init__(self, message: str = f"Invalid user field. "
                                      f"{follow_user_manual_message}"):
        self.message = message
        self.error_code = error_codes.invalid_user_fields


class ExperimentException(XprExceptions):
    """
    Occurs if there is any issue with fields in user info
    """

    def __init__(self, message: str = "Unable to insert record in database"):
        self.message = message
        self.error_code = error_codes.experiment_failed


class ComponentException(XprExceptions):
    """
    Occurs if there is any issue with fields in user info
    """

    def __init__(self, message: str = "Unable to perform the operation"):
        self.message = message
        self.error_code = error_codes.experiment_failed


class CodeRepoManagerException(XprExceptions):
    """
    raised in case of invalid code_repo_manager
    """

    def __init__(self, message: str = "Invalid code repo manager"):
        self.message = message
        self.error_code = error_codes.invalid_code_repo_manager


class BuildProjectInputException(XprExceptions):
    """
    raised if there are errors in input for build project
    """

    def __init__(self, message: str = "Invalid input for build project"):
        self.message = message
        self.error_code = error_codes.invalid_build_project_input


class ProjectCorruptedException(XprExceptions):
    """
    raised if the project info on db is corrupted
    """

    def __init__(self, message: str = "Project info is corrupted. "
                                      "Contact Xpresso team"):
        self.message = message
        self.error_code = error_codes.project_corrupted_exception


class BuildManagerException(XprExceptions):
    """
    raised in case of invalid type provided for a factory class
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.build_manager_exception


class JenkinsConnectionFailedException(BuildManagerException):
    """
    This exception is thrown when Jenkins connection is failed
    """


class JenkinsInvalidInputException(BuildManagerException):
    """
    This exception is thrown when Jenkins connection is failed
    """


class RepoManagerException(XprExceptions):
    """
    raised in case deletion of bitbucket repo throws any error
    """

    def __init__(self, message: str,
                 status_code: int = 400):
        self.message = message
        self.error_code = error_codes.code_repo_manager_exception
        self.status_code = status_code


class UnsupportedFlavorException(XprExceptions):
    """
    Occurs if a factory class is trying to create an unsupported base class
    """

    def __init__(self, message: str = "The flavor that you have mentioned is "
                                      "not supported."):
        self.message = message
        self.error_code = error_codes.unsupported_deployment_flavor


class InputLengthMismatch(XprExceptions):
    """
    Occurs if x-y inputs for plots mismatch
    """

    def __init__(self,
                 message: str = "X and Y axis input length doesn't match"):
        self.message = message
        self.error_code = error_codes.input_length_mismatch


class PipelineRunException(XprExceptions):
    """
    Occurs when unable to fetch some parameter for a particular pipeline run
    from the database
    """

    def __init__(self,
                 message: str = "No entry present in the database for the "
                                "provided run id"):
        self.message = message
        self.error_code = error_codes.record_not_found


class AttributeNotPresent(XprExceptions):
    """
    Occurs if Attribute not present in dataset input
    """

    def __init__(self,
                 message: str = "Attribute column not present in the input"):
        self.message = message
        self.error_code = error_codes.invalid_attribute


class ExperimentControlException(XprExceptions):
    """
    Occurs if any exception is caught during experiment control
    """

    def __init__(self, message: str = "Experiment control failed"):
        self.message = message
        self.error_code = error_codes.experiment_control_failed


class ServiceMeshCreationException(XprExceptions):
    """
        This exception is called when the mesh info provided is incorrect
    """

    def __init__(self, message: str = "One or more service mesh default "
                                      "keys were specified incorrectly. "
                                      f"{follow_project_manual_message}"):
        self.message = message
        self.error_code = error_codes.service_mesh_creation_failed


class ServiceMeshDeploymentFailed(XprExceptions):
    """
            This exception is called when the mesh deployment fails
        """

    def __init__(self, message: str = "Service Mesh deployment failed."):
        self.message = message
        self.error_code = error_codes.service_mesh_deployment_failed


class UndeployServiceMeshFailed(XprExceptions):
    """
        This exception is called when the mesh undeploy fails
    """

    def __init__(self, message: str = "Service Mesh undeploy failed."):
        self.message = message
        self.error_code = error_codes.service_mesh_undeploy_failed


class InvalidDateFormat(XprExceptions):
    """
    Occurs when date format is invalid
    """

    def __init__(self,
                 message: str = "Invalid date format"):
        self.message = message
        self.error_code = error_codes.invalid_date


class FieldTypeException(XprExceptions):
    """
    Occurs if data type of any XprObject field is invalid or incorrect
    """

    def __init__(self, field: str, valid_type: str):
        self.message = f"Invalid datatype passed for `{field}` field." \
                       f"Valid datatype for this field is {valid_type}"
        self.error_code = error_codes.invalid_field_type


class ExperimentFieldsException(XprExceptions):
    """
    Occurs if there is any issue with fields in experiment
    """

    def __init__(self, message: str = "Invalid experiment fields"):
        self.message = message
        self.error_code = error_codes.experiment_field_error


class InferenceServiceDeploymentFailed(XprExceptions):
    """ Occurs when inference service deployment fails """

    def __init__(self, message: str = "Inference service deployment failed."):
        self.message = message
        self.error_code = error_codes.inference_service_deployment_failed


class PipelineComponentException(XprExceptions):
    """ Occurs when pipeline component creation fails """

    def __init__(self, message: str = "Unable to create pipeline component."):
        self.message = message
        self.error_code = error_codes.required_parameter_not_found


class RunInfoException(XprExceptions):
    """Occurs if there is any error in run info"""

    def __init__(self, message: str = "Invalid run information."):
        self.message = message
        self.error_code = error_codes.run_field_error


class LdapUserDeletionException(XprExceptions):
    """Occurs when deletion of user from ldap directory failed"""

    def __init__(self,
                 message: str = "Unable to delete user from ldap directory"):
        self.message = message
        self.error_code = error_codes.ldap_user_addition_failed


class SparkSubmitException(XprExceptions):
    """ Occurs remote submit of spark jobs fails """

    def __init__(self, message: str = "Unable to submit spark job."):
        self.message = message
        self.error_code = error_codes.project_deployment_failed


class ControllerAPICallException(XprExceptions):
    """
    Occurs when call to a controller api fails
    """

    def __init__(self, message: str, api_error_code: int):
        self.message = message
        self.error_code = api_error_code


class ServiceAccountCreationFailed(XprExceptions):
    """
    Thrown when kubernetes api fails to create service account
    """

    def __init__(self, message: str = "Service Account creation failed"):
        self.message = message
        self.error_code = error_codes.service_account_creation_failed


class RoleBindingCreationFailed(XprExceptions):
    """
    Thrown when kubernetes api fails to create rolebinding
    """

    def __init__(self, message: str = "Rolebinding creation failed"):
        self.message = message
        self.error_code = error_codes.service_account_creation_failed


class DataConnectionsException(XprExceptions):
    """
    Thrown when Data Connections fails to connect to a data source
    """

    def __init__(self,
                 message: str = "Data connections failed to connect to a source"):
        self.message = message
        self.error_code = error_codes.connections_failed_to_connect


class InvalidDictionaryException(XprExceptions):
    """
    Thrown when invalid key-value pair passed to a method call in Data connections
    """

    def __init__(self,
                 message: str = "Invalid Key-Value pair passed to method call"):
        self.message = message
        self.error_code = error_codes.invalid_key_value_pair


class InvalidPathException(XprExceptions):
    """
    Thrown when invalid path passed to a method call in Data connections
    """

    def __init__(self, message: str = "Invalid path passed to method call"):
        self.message = message
        self.error_code = error_codes.invalid_path


class FSClientCreationFailed(XprExceptions):
    """
    Thrown when file system client creation causes any exceptions
    """

    def __init__(self,
                 message: str = "File system client creation causes an exception"):
        self.message = message
        self.error_code = error_codes.fs_client_creation_failed


class FSClientListDirectoryFailed(XprExceptions):
    """
    Thrown when file system client fails to return a list of files/directories
    """

    def __init__(self, message: str = "File system client failed to "
                                      "return a list of files/directories"):
        self.message = message
        self.error_code = error_codes.fs_client_list_dir_failed


class FSClientUnsupportedFileType(XprExceptions):
    """
    Thrown when an unsupported file type is passed to file system client
    """

    def __init__(self, message: str = "Unsupported file type passed"
                                      " to file system client"):
        self.message = message
        self.error_code = error_codes.fs_client_unsupported_file_type


class InvalidMethodCall(XprExceptions):
    """
    Thrown when an invalid method call is made. Like method call to import files
    from a database connector.
    """

    def __init__(self, message: str = "Invalid method call made."):
        self.message = message
        self.error_code = error_codes.invalid_method_call


class DBConnectionFailed(XprExceptions):
    """
    Thrown when connection to database is failed.
    """

    def __init__(self, message: str = "Connection to database failed."):
        self.message = message
        self.error_code = error_codes.connection_to_database_failed


class BQConnectionFailed(XprExceptions):
    """
    Thrown when connection to google bigquery is failed.
    """

    def __init__(self, message: str = "Connection to Google BigQuery failed."):
        self.message = message
        self.error_code = error_codes.connection_to_bigquery_failed


class BQQueryJobFailed(XprExceptions):
    """
    Thrown when query job given to bigquery fails.
    """

    def __init__(self, message: str = "Query job to Google BigQuery failed."):
        self.message = message
        self.error_code = error_codes.query_job_to_bigquery_failed


class HDFSManagerException(XprExceptions):
    """
    Thrown when unable to perform an operation using HDFS Manager.
    """

    def __init__(self, message: str = "unable to perform the operation."):
        self.message = message
        self.error_code = error_codes.unsuccessful_connection


class RunException(XprExceptions):
    """
    Throws this exception in case anything fails during a run
    """

    def __init__(self, message: str = "Experiment run failed."):
        self.message = message
        self.error_code = error_codes.run_failed


class FileExistsException(XprExceptions):
    """
    File already exists at provided location
    """

    def __init__(self, file_path: str):
        self.message = f"File operation prohibited. A file/folder already" \
                       f" exists at this location: {file_path}"
        self.error_code = error_codes.file_exists_error


class UnsupportedAPIRequestType(XprExceptions):
    """
    API request type not supported
    """

    def __init__(self, msg: str = 'Unsupported request type'):
        self.message = msg
        self.error_code = error_codes.unsupported_request_type


class APIRouteNotRegistered(XprExceptions):
    """
    API route is not configured/registred in resolver config
    """

    def __init__(self, msg: str = 'API route not registred in config. Contact '
                                  'xpresso team.'):
        self.message = msg
        self.error_code = error_codes.route_not_registred_in_config


class UnsupportedNotifierType(XprExceptions):
    """
    Notifier type not supported
    """

    def __init__(self, message=f"Notifier type not supported"):
        self.message = message
        self.error_code = error_codes.unsupported_notifier_type


class ValidatorNotAvailable(XprExceptions):
    """
    No validator available for xpresso operation
    """

    def __init__(self, message=f"Validator not available"):
        self.message = message
        self.error_code = error_codes.validator_not_available


class RequestDispatcherException(XprExceptions):
    """
    No validator available for xpresso operation
    """

    def __init__(self, message=f"Request not accepted.", error_code=error_codes.request_despatch_error):
        self.message = message
        self.error_code = error_code


class IpynbParseException(XprExceptions):
    """
    raised whenjupyter notebook parsing fails experiment fails
    """

    def __init__(self, message=f'Notebook parse exception'):
        self.message = message
        self.error_code = error_codes.jupyter_exp_error


class FileCopyException(XprExceptions):
    """
    Failed to copy file to the target location
    """

    def __init__(self, source_path: str, dest_path: str):
        self.message = f"Copy operation prohibited. Unable to copy file" \
                       f"from {source_path} to: {dest_path}"
        self.error_code = error_codes.copy_file_error


class JupyterExpException(XprExceptions):
    """
    raised something goes wrong while processing jupyter notebook
    """

    def __init__(self, message=f'Could not process notebook'):
        self.message = message
        self.error_code = error_codes.jupyter_exp_error

class JupyterhubSetupException(XprExceptions):
    """
    Exception raised when user/project setup fails in jupyterhub
    """

    def __init__(self, message=f'Failed to Jupyterhub user/project setup'):
        self.message = message
        self.error_code = error_codes.jupyterhub_setup

class UnsupportedComponent(XprExceptions):
    """
    raised when mount path specified for image save is None
    """

    def __init__(self, message=f'Specified Component type is currently unsupported'):
        self.message = message
        self.error_code = error_codes.unsupported_component


class MountPathNoneType(XprExceptions):
    """
    raised when mount path specified for image save is None
    """

    def __init__(self, message="Mount path not specified during deployment"):
        self.message = message
        self.error_code = error_codes.mount_path_error


class FileExplorerException(XprExceptions):
    """
    Occurs if any errors occur in file explorer module
    """

    def __init__(self, message: str):
        self.error_code = error_codes.file_explorer_error
        self.message = message


class IllegalRequestState(XprExceptions):
    """
    illegal request state
    """

    def __init__(self, msg: str = 'Illegal request state'):
        self.message = msg
        self.error_code = error_codes.illegal_request_state


class SchedulingException(XprExceptions):
    """
    Raised if any error occurs in scheduling
    """

    def __init__(self, message: str):
        self.error_code = error_codes.scheduling_error
        self.message = message


class DuplicateExperimentException(XprExceptions):
    """
    Occurs if there is a request to create duplicate experiment
    """

    def __init__(self, message: str):
        self.error_code = error_codes.duplicate_experiment_error
        self.message = message


class BranchTypeException(XprExceptions):
    """
    Occurs if the branch_type value provided is not valid
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.branch_type_error


class XprFieldException(XprExceptions):
    """
    This exception is raised If a xpr field is incorrect is invalid
    """

    def __init__(self, field: str):
        self.message = f"`{field}` field is not a valid/accepted input" \
                       f" for this operation."
        self.error_code = error_codes.invalid_xpr_field


class PolicyException(XprExceptions):
    """
    This exception is thrown when there is an issue with instance policy
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.instance_policy_error


class BaseImageException(XprExceptions):
    """
    This exception is raised when there is any issues with base image
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.base_image_error


class BaseImageNotFoundException(XprExceptions):
    """
    This exception is raised if the image is not found
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.base_image_not_found


class AuthenticationManagerException(XprExceptions):
    """
    raised in case of invalid type provided for a factory class of
    Authentication Manager
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.authentication_manager_exception


class ExpiredPasswordException(XprExceptions):
    """
    Class to define an exception thrown when the temporary password
    has expired
    """

    def __init__(self, message: str = "Your Temporary password has expired. "
                                      "Please reset your password again."):
        self.message = message
        self.error_code = error_codes.expired_password


class InvalidMetrics(XprExceptions):
    """
        This exception is thrown when there are issues in metrics specified
    """

    def __init__(self, message: str = "Metrics specified are invalid."):
        self.message = message
        self.error_code = error_codes.metrics_invalid


class ReportKPIsFailed(XprExceptions):
    """This exception is thrown when the report_metrics API fails"""

    def __init__(self, message: str = "Report KPIs failed."):
        self.message = message
        self.error_code = error_codes.report_kpis_failed


class MetricsNotFound(XprExceptions):
    """This exception is thrown when the metrics are fetched
    before being updated in the DB"""

    def __init__(self, message: str = "Metrics not found."):
        self.message = message
        self.error_code = error_codes.metrics_not_found


class ModelNameException(XprExceptions):
    """
    occurs if the project name is invalid
    """

    def __init__(self, message: str = ""):
        model_name_format = f"Model name format is invalid."
        self.message = message or model_name_format
        self.error_code = error_codes.invalid_model_name


class ModelDeploymentFailedException(XprExceptions):
    """
    Class to define an exception thrown when model deployment fails
    """

    def __init__(self, message: str = "Model deployment failed."):
        self.message = message
        self.error_code = error_codes.model_deployment_failed


class ABTestingFailedException(XprExceptions):
    """
    Class to define an exception thrown when A/B Testing fails
    """

    def __init__(self, message: str = "Model deployment failed."):
        self.message = message
        self.error_code = error_codes.ab_testing_failed


class NfsFileAPIException(XprExceptions):
    """
    This exception is raised when the api request to nfs service has failed
    """
    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.nfs_file_api_error


class MarketPlaceNotFoundException(XprExceptions):
    """
    This exception will be raised when a given Marketplace component does not exist in DB
    """
    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.base_image_not_found


class MarketplaceInputException(XprExceptions):
    """
    This is raised when the input for marketplace operations is incorrect
    """
    def __init__(self, message: str):
        self.error_code = error_codes.marketplace_input_error
        self.message = message


class PromotionFailedException(XprExceptions):
    """
    This exception is raised when inter-instance deployment transfer fails
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.deployment_promotion_failed


class ModelNotFoundException(XprExceptions):
    """
        This exception is raised when the api request to model API has failed
        """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.model_not_found


class ABTestingNotFound(XprExceptions):
    """
      This exception is raised when the api request to A/B testing API has failed
    """

    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.ab_testing_not_found


class UndefinedRoleException(XprExceptions):
    """
    Class to define an exception thrown when roles is undefined
    """

    def __init__(self, message: str = "Role is not present in registry."):
        self.message = message
        self.error_code = error_codes.undefined_role


class RequestNotAllowedException(XprExceptions):
    """
    Class to define an exception thrown when roles is undefined
    """

    def __init__(self, message: str = "This request is not allowed."):
        self.message = message
        self.error_code = error_codes.request_not_allowed


class DockerOperationFailedException(XprExceptions):
    """
    This exception is raised when a docker related operation fails
    """
    def __init__(self, message: str):
        self.message = message
        self.error_code = error_codes.docker_operation_failed


class RoleFormatException(XprExceptions):
    """
    This exception is called when the request for role manager
    has invalid format
    """

    def __init__(self, message: str = "Role format is invalid."):
        self.message = message
        self.error_code = error_codes.invalid_role_format


class JupyterIntegrationManagerException(XprExceptions):
    """
    raised in case of invalid Jupyter Integration Manager initialization
    """

    def __init__(self, message: str = "Invalid class initialization"):
        self.message = message
        self.error_code = error_codes.jupyter_integration_manager_exp

class JupyterIntegrationValidatorException(XprExceptions):
    """
    raised in case of invalid Jupyter Integration Manager initialization
    """

    def __init__(self, message: str = "Invalid input json for request"):
        self.message = message
        self.error_code = error_codes.jupyter_integration_validator_exp


class TermsNotAcceptedException(XprExceptions):
    """
    Class to define an exception thrown when an terms are
    not accepted by a user
    """

    def __init__(self, message: str = "Unable to redirect, please accept "
                                      "terms and conditions to process"):
        self.message = message
        self.error_code = error_codes.terms_not_accepted


class LogsNotFoundException(XprExceptions):
    """
    class for exception thrown when the requested logs are not found in the
    elasticsearch.
    """

    def __init__(self, message: str = "Logs not found"):
        self.message = message
        self.error_code = error_codes.logs_not_found


class IndexNotFoundException(XprExceptions):
    """
    class for exception thrown when the requested index does not exist in the
    elasticsearch.
    """

    def __init__(self, message: str = "Index not found"):
        self.message = message
        self.error_code = error_codes.index_not_found


class RequiredParameterMissingException(XprExceptions):
    """
    class for exception thrown when the required parameter(s) is/are missing in the
    request.
    """

    def __init__(self, message: str = "Required parameter not found"):
        self.message = message
        self.error_code = error_codes.required_parameter_missing


class PageNotFoundException(XprExceptions):
    """
    class for exception thrown when the requested page is not found.
    """

    def __init__(self, message: str = "Page not found"):
        self.message = message
        self.error_code = error_codes.page_not_found


class ServiceNotReachableException(XprExceptions):
    """
    class for exception thrown when a service is not reachable.
    """

    def __init__(self, message: str = "Service unreachable"):
        self.message = message
        self.error_code = error_codes.service_not_reachable


class LicenseExpiredException(XprExceptions):
    """
        class for exception thrown when license is expired.
        """

    def __init__(self, message: str = "License has expired"):
        self.message = message
        self.error_code = error_codes.license_expired


class InvalidLicenseKeyException(XprExceptions):
    """
        class for exception thrown when invalid license key is provided.
        """

    def __init__(self, message: str = "Invalid license key provided"):
        self.message = message
        self.error_code = error_codes.invalid_license_key


class ModelMonitoringServiceFailed(XprExceptions):
    """
        This exception is raised when API requests to Model Monitoring fail
    """

    def __init__(self, message: str = "Request to Model Monitoring failed"):
        self.message = message
        self.error_code = error_codes.model_monitoring_request_failed


class InvalidModelMonitoringData(XprExceptions):
    """
        This exception is raised when data passed on for model
        monitoring does not match a particular format
    """
    def __init__(self, message: str = "Invalid schema defined for model"
                                      "monitoring"):
        self.message = message
        self.error_code = error_codes.model_manager_exception


class InvalidMessageQueueException(XprExceptions):
    """
    Class to define an exception thrown when the temporary password
    has expired
    """

    def __init__(self, message: str = "Invalid Type Provided for Message "
                                      "Queue."):
        self.message = message
        self.error_code = error_codes.model_manager_exception

"""
    Utility module for XJP python scripts
"""


__author__ = ["Shlok Chaudhari"]


import os
import requests
from xpresso.ai.core.commons.utils.generic_utils import str_hash256
from xpresso.ai.core.commons.utils.user_utils import save_token, get_token_file_path


def is_token_invalid(token: str, controller_url: str) -> bool:
    """
        Validate given login token
    Args:
        token(str): login token
        controller_url(str): controller URL
    Returns:
        session_expiry(bool): True if expired
    """
    url = f"{controller_url}/users/details"
    r = requests.get(url, headers={"token": token}, verify=False)
    if r.json()['outcome'] == 'failure':
        return True
    return False


def get_token_from_file(client_path) -> str:
    """
        Get token from file
    Returns:
        token(str)
    """
    base_path = os.path.join(os.path.expanduser('~'), client_path)
    token_file_path = '{}.current'.format(base_path)

    token = None
    current_user = os.getenv('CURRENT_USER', '')
    current_user_enc = str_hash256(current_user)
    if current_user and current_user_enc and current_user_enc not in token_file_path:
        parts = token_file_path.split('/')
        parts.insert(-1, current_user_enc)
        token_file_path = '/'.join(parts)
    try:
        with open(token_file_path, "r") as f:
            token = f.read()
            token = token.strip("\\n")
    except FileNotFoundError:
        return None
    return token


def save_access_token_in_file(client_path, access_token=None) -> str:
    """
        Save login token in file
    Args:
        client_path
        access_token(str): login token
    """
    if not access_token:
        return
    path = os.path.join(os.path.expanduser('~'), client_path)
    token_file = get_token_file_path(path)
    if os.path.exists(token_file):
        os.remove(token_file)
    os.makedirs(os.path.dirname(token_file), exist_ok=True, mode=0o755)
    save_token(token_file, access_token)


def get_response_box(resp: str) -> str:
    """
        Concatenates response in dialog box and returns it
    Returns:
        response dialog box in javascript(str)
    """
    return """require(
          ["base/js/dialog"],
          function(dialog) {
              dialog.modal({
                  title: i18n.msg._('Server response'),
                  sanitize: false,
                  body: '""" + resp + """',
                  buttons: {
                      'OK': {
                        'class': 'btn-default',
                        'click': function() {
                            Jupyter.notebook.delete_cells([0]);
                            Jupyter.keyboard_manager.enable();
                        }
                      }
                  },
                  open: function() {
                    $('.modal-header').children('button').click(function(){
                      Jupyter.notebook.delete_cells([0]);
                      Jupyter.keyboard_manager.enable();
                    });
                    Jupyter.keyboard_manager.disable();
                  }
              });
          }
        );
        """

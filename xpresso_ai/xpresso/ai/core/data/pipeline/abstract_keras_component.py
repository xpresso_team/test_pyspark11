"""
This is the implementation of keras component
"""

__all__ = ["AbstractKerasComponent"]

import os
import sys

import numpy as np
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING, \
    DEFAULT_PREDICTION_THRESHOLD, DEFAULT_VALIDATION_SIZE
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.ml_utils.generic import MODEL_TYPE_ARG, \
    VALIDATION_SIZE_ARG, GENERATE_VALIDATION_METRICS_ARG, \
    PREDICTION_THRESHOLD_ARG, OUTPUT_PATH_ARG
from xpresso.ai.core.ml_utils.keras import KerasMetrics, KerasPlots
from xpresso.ai.core.ml_utils.shap import ShapUtils, SHAP_EXPLAINER_ARG, \
    SHAP_EXPLAINING_SET_SIZE_ARG, SHAP_N_INDIVIDUAL_INSTANCE_ARG, \
    SHAP_N_TOP_FEATURES_ARG, SHAP_N_TOP_INTERACTION_FEATURES_ARG, \
    SHAP_LINK_ARG, \
    SHAP_FEATURE_NAMES_ARG, SHAP_FOLDER


class AbstractKerasComponent(AbstractPipelineComponent):
    """ Main class for any keras pipeline-job. It is extended from
    AbstractPipelineComponent.
    This allows to report metrics for trained keras model
    """

    def __init__(self, name=None):
        super().__init__(name)
        self.model = None
        self.x_train = None
        self.y_train = None
        self.history = None

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
        """
        try:
            if self.x_train is None or self.y_train is None or not self.model:
                print("Model, x_train or y_train data not "
                      "provided for metric calculation")
            else:
                self.report_keras_metrics(self.x_train, self.y_train)
                self.generate_shap_explainability(self.x_train)
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def report_keras_metrics(self, x_values, y_values):
        """
        The method automatically detects user's model type (
        xgboost.XGBRegressor, xgboost.XGBClassifier) and collects, calculates
        and reports all metrics available in SKlearn library
        Args:
            x_values : array_like or sparse matrix, shape (n_samples,
                n_features) input matrix (either training set, validation
                set, or test set)
            y_values : array-like of shape (n_samples,) or (n_samples,
                n_classes) target variable vector or matrix (either training
                set, validation set, or test set)
        """
        keras_metrics = KerasMetrics(self.model, self.run_parameters)
        calculated_metrics = keras_metrics.get_all_metrics(x_values,
                                                           y_values)
        print("Reporting metrics to xpresso.ai")
        # send metrics to xpresso UI
        self.report_kpi_metrics(calculated_metrics)
        self.logger.info("Metrics Reported")
        if self.history:
            KerasPlots().plot_learning_curve(self.history)

    def generate_shap_explainability(self, x_values):
        """
        Create Shapley plots for the trained model.

        Allows an approximation of shapley values for a given
        model based on a number of samples from data source X
        Args:
            x_values (dataframe, numpy array, or pixel_values) :
                dataset to be explained
        """
        explainer_list = [
            'DeepExplainer', 'GradientExplainer', 'KernelExplainer',
            'SamplingExplainer']
        shap_utils = ShapUtils(self.model, explainer_list, self.run_parameters)
        shap_utils.explain_shap(x_values)


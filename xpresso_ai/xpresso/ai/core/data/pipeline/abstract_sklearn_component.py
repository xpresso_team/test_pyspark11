"""
This is the implementation of sklearn component
"""

__all__ = ["AbstractSklearnComponent"]

import sys

from xpresso.ai.core.commons.utils.constants import DEFAULT_VALIDATION_SIZE, \
    EMPTY_STRING
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.ml_utils.generic import MODEL_TYPE_ARG, \
    VALIDATION_SIZE_ARG, GENERATE_VALIDATION_METRICS_ARG, OUTPUT_PATH_ARG
from xpresso.ai.core.ml_utils.shap import ShapUtils, SHAP_EXPLAINER_ARG, \
    SHAP_EXPLAINING_SET_SIZE_ARG, \
    SHAP_FEATURE_NAMES_ARG, SHAP_LINK_ARG, SHAP_N_INDIVIDUAL_INSTANCE_ARG, \
    SHAP_N_TOP_FEATURES_ARG, SHAP_N_TOP_INTERACTION_FEATURES_ARG, SHAP_FOLDER
from xpresso.ai.core.ml_utils.sklearn import SklearnMetrics


class AbstractSklearnComponent(AbstractPipelineComponent):
    """ Main class for any sklearn pipeline-job. It is extended from
    AbstractPipelineComponent.
    This allows to report metrics for trained sklearn model
    """

    def __init__(self, name, run_name, params_filename,
                 params_commit_id):
        super().__init__(name=name,
                         run_name=run_name,
                         params_filename=params_filename,
                         params_commit_id=params_commit_id)
        self.model = None
        self.x_train = None
        self.y_train = None

    def completed(self, push_exp=False, success=True):
        """
        The completed method for sklearn component calls the
        report_sklearn_metrics
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
        """
        try:
            if self.x_train is None or self.y_train is None or not self.model:
                print("Model, x_train or y_train data not "
                      "provided for metric calculation")
            else:
                self.report_sklearn_metrics(self.x_train, self.y_train)
                self.generate_shap_explainability(self.x_train)
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def report_sklearn_metrics(self, x_values, y_values):
        """
        The report_metrics method collects, calculate and report all metrics
        available in SKlearn library.
        The method detects user's model type (regressor, classifiers-binary,
        classifiers-multiclass, classifiers-multilabel, unsupervised-clustering)
        Args:
            x_values (array_like or sparse matrix): input matrix (either
                training set, validation set, or test set) with shape of (
                n_samples, n_features)
            y_values : array-like of shape (n_samples,) or (n_samples,
                n_classes) target variable vector or matrix (either training
                set, validation set, or test set)
        """
        sklearn_metrics = SklearnMetrics(self.model, self.run_parameters)
        calculated_metrics = sklearn_metrics.get_all_metrics(x_values, y_values)
        print("Reporting metrics to xpresso.ai")
        # send metrics to xpresso UI
        self.report_kpi_metrics(calculated_metrics)
        self.logger.exception("Metrics Reported")

    def generate_shap_explainability(self, x_values):
        """
        Create Shapley plots for the trained model.
        Allows an approximation of shapley values for a given
        model based on the number of samples from data source X
        Args:
            x_values (dataframe, numpy array, or pixel_values) :
            dataset to be explained
        """
        explainer_list = [
            'TreeExplainer', 'LinearExplainer', 'DeepExplainer',
            'GradientExplainer', 'KernelExplainer', 'SamplingExplainer']
        shap_utils = ShapUtils(self.model, explainer_list, self.run_parameters)
        shap_utils.explain_shap(x_values)

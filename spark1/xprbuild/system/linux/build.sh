#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

pip3 install pylint pytest pytest-cov
# Build the dependencies
pip3 install --trusted-host 172.16.5.71  --index-url http://172.16.5.71:30088/simple/ --extra-index-url  https://pypi.python.org/pypi -r ${ROOT_FOLDER}/requirements/requirements.txt
pip3 install --trusted-host 172.16.5.71  --index-url http://172.16.5.71:30088/simple/ --extra-index-url  https://pypi.python.org/pypi -r ${ROOT_FOLDER}/xpresso_ai/requirements.txt